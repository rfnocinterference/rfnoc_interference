
"""Classes for reading/writing AXI4-Stream interfaces."""
# The MIT License
#
# Copyright (c) 2017-2018 by the author(s)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# Author(s):
#   - Andreas Oeldemann <andreas.oeldemann@tum.de>
#   - Stefan Jahn       <stefan.jahn@students.fh-hagenberg.at
#
# Description:
#
# Classes for reading/writing AXI4-Stream interfaces.

import cocotb
from cocotb.triggers import RisingEdge, Lock
from cocotb.result import ReturnValue
import random


class AXIS_Writer:
    """AXI4-Stream interface writer."""

    def connect(self, dut, name, clk, bit_width,postfix="", prefix=""):
        """Connect the DuT AXI-Stream interface to this writer.
        When parameter 'prefix' is not set, DUT AXI-Stream signals are expected
        to be named s_axis_tdata, s_axis_tvalid, ... If 'prefix' is set, DuT
        AXI4-Stream signals are expected to be named s_axis_<prefix>_tdata, ...
        """

        self.bit_width = bit_width


        self.clk = clk
        self.s_axis_tdata = getattr(dut, "%stdata%s" % (prefix,postfix))
        self.s_axis_tvalid = getattr(dut, "%stvalid%s" % (prefix,postfix))
        self.s_axis_tlast = getattr(dut, "%stlast%s" % (prefix,postfix))
        
        # make tkeep as optional, because in ettus rfnoc axi bus it will not be needed
        try:
            self.s_axis_tkeep = getattr(dut, "%stkeep%s" % (prefix,postfix))
            self.has_tkeep = True
        except AttributeError:
            self.has_tkeep = False

        # flow control (tready) is optional
        try:
            self.s_axis_tready = getattr(dut, "%stready%s" % (prefix,postfix))
            self.has_tready = True
        except AttributeError:
            self.has_tready = False

        # tuser is optional
        try:
            self.s_axis_tuser = getattr(dut, "%stuser%s" % (prefix,postfix))
            self.has_tuser = True
        except AttributeError:
            self.has_tuser = False

        self.async_write_lock = Lock("%s_write_lock" % name)
        self.async_write_started = False



    @cocotb.coroutine
    def rst(self):
        """Reset signals."""
        self.s_axis_tdata <= 0
        self.s_axis_tvalid <= 0
        self.s_axis_tlast <= 0
        if self.has_tkeep:    
            self.s_axis_tkeep <= 0
        if self.has_tuser:
            self.s_axis_tuser <= 0

        yield RisingEdge(self.clk)

    @cocotb.coroutine
    def write_sync(self, tdata, tkeep=None, tuser=None, insert_random_gaps=False):
        """Perform write on AXI4-Stream slave interface.
        Writes a complete transfer on the AXI4-Stream slave interface. The
        function expects a list of the TDATA words and the value of the TKEEP
        signal for the last word transfer. Optionally, a list of TUSER values
        can be specified. If the insert_random_gaps parameter is set to True
        (which is the default), random idle gaps are inserted by setting
        TVALID low.
        """
        

        for i, word in enumerate(tdata):
            self.s_axis_tdata <= word
            self.s_axis_tvalid <= 1
    
            if self.has_tuser:
                if i < len(tuser):
                    self.s_axis_tuser <= tuser[i]
                else:
                    self.s_axis_tuser <= 0

            if i == len(tdata)-1:
                self.s_axis_tlast <= 1
                if self.has_tkeep == 1:
                    self.s_axis_tkeep <= tkeep
            else:
                if self.has_tkeep == 1:
                    self.s_axis_tkeep <= pow(2, self.bit_width/8)-1

            while True:
                yield RisingEdge(self.clk)
                if not self.has_tready or int(self.s_axis_tready) == 1:
                    break

            if insert_random_gaps:
                # with a chance of 20%, insert a clock cycle in which no data
                # is ready to be transmitted by setting tvalid low
                if i != len(tdata)-1 and random.random() < 0.2:
                    self.s_axis_tvalid <= 0
                    yield RisingEdge(self.clk)

        self.s_axis_tvalid <= 0
        self.s_axis_tlast <= 0
     
        
    
    @cocotb.coroutine
    def write_async(self, tdata, sync = True, tkeep=None, tuser=None, insert_random_gaps=False):
        
        yield self.async_write_lock.acquire()
        yield self.join()
        self.async_write_started = True

        print("asyncwrite started")

        if sync:
            yield RisingEdge(self.clk)
        
        self.c_send = cocotb.fork(self.write_sync(tdata,tkeep,tuser,insert_random_gaps))
        self.async_write_lock.release()
        print("asyncwrite leave")

    @cocotb.coroutine
    def join(self):
        if self.async_write_started:
            yield self.c_send
            self.async_write_started = False

class AXIS_Reader(object):
    """AXI4-Stream interface reader."""

    def connect(self, dut, name, clk, bit_width,postfix="", prefix=""):
        """Connect the DuT AXI-Stream interface to this reader.
        When parameter 'prefix' is not set, DUT AXI-Stream signals are expected
        to be named m_axis_tdata, m_axis_tvalid, ... If 'prefix' is set, DuT
        AXI4-Stream signals are expected to be named m_axis_<prefix>_tdata, ...
        """
        self.bit_width = bit_width


        self.clk = clk
        self.m_axis_tdata = getattr(dut, "%stdata%s" % (prefix,postfix))
        self.m_axis_tvalid = getattr(dut, "%stvalid%s" % (prefix,postfix))
        self.m_axis_tlast = getattr(dut, "%stlast%s" % (prefix,postfix))

        try:
            self.m_axis_tkeep = getattr(dut, "%stkeep%s" % (prefix,postfix))
            self.has_tkeep = True
        except AttributeError:
            self.has_tkeep = False

        # flow control (tready) is optional
        try:
            self.m_axis_tready = getattr(dut, "%stready%s" % (prefix,postfix))
            self.has_tready = True
        except AttributeError:
            self.has_tready = False

        # tuser is optional
        try:
            self.m_axis_tuser = getattr(dut, "%stuser%s" % (prefix,postfix))
            self.has_tuser = True
        except AttributeError:
            self.has_tuser = False

    @cocotb.coroutine
    def rst(self):
        """Reset signals."""
        if self.has_tready:
            self.m_axis_tready <= 0
        yield RisingEdge(self.clk)

    @cocotb.coroutine
    def read(self):
        """Perform read on AXI4-Stream master interface.
        Reads a complete transfer on the AXI4-Stream master interface. The
        function returns a list of the TDATA words and the value of the TKEEP
        signal for the last word transfer. If a side-band TUSER interface is
        present, a list of the values is returned as well.
        """
        print("start read")
        # empty tdata list
        tdata = []
        self.m_axis_tready <= 1
        print("tready set to 1")
        if self.has_tuser:
            # empty tuser list
            tuser = []
        else:
            tuser = None


        while True:
            yield RisingEdge(self.clk)

            if (not self.has_tready or int(self.m_axis_tready)) and \
                    int(self.m_axis_tvalid):

                tdata.append(int(self.m_axis_tdata))

                if self.has_tuser:
                    tuser.append(int(self.m_axis_tuser))

                if int(self.m_axis_tlast):
                    if self.has_tkeep:
                        tkeep = int(self.m_axis_tkeep)
                    else:
                        tkeep = 1
                    break

                else:
                    if self.has_tkeep:
                        if int(self.m_axis_tkeep) != pow(2, self.bit_width/8)-1:
                            raise cocotb.result.TestFailure("invalid AXI4-Stream " +
                                                    "TKEEP signal value")

        raise ReturnValue((tdata, tkeep, tuser))