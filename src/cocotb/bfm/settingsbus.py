
import cocotb
from cocotb.drivers import BusDriver
from cocotb.decorators import coroutine
from cocotb.triggers import RisingEdge, FallingEdge, ReadOnly, NextTimeStep, Event
from cocotb.result import TestError
from cocotb.monitors import BusMonitor


class SettingsBusMaster(BusDriver):
    _signals = {"set_addr":"sb_addr_i", "set_stb":"sb_strobe_i", "set_data":"sb_data_i", "rb_data":"sb_rbdata_o"}
    _optional_signals = {"rb_strobe":"rbstrobe_o"}

    def __init__(self,entity: cocotb.handle.HierarchyObject, name: str, clock:cocotb.handle.ModifiableObject,data_bitwidth:int,address_bitwidth:int,**kwargs):
        BusDriver.__init__(self,entity,name,clock,**kwargs)

        self.data_bitwidth=data_bitwidth
        self.address_bitwidth = address_bitwidth
        
        # drive defaults
        self.bus.set_addr.setimmediatevalue(0)
        self.bus.set_stb.setimmediatevalue(0)
        self.bus.set_data.setimmediatevalue(0)

        self.log.debug("SettingsBusMaster created")


    @cocotb.coroutine
    def reset(self, sync = True):
        if sync:
            yield RisingEdge(self.clock)

        self.bus.set_addr <= 0
        self.bus.set_stb  <= 0
        self.bus.set_data <= 0

    @cocotb.coroutine
    def write(self, address: int, data: int):
        yield RisingEdge(self.clock)

        if(data >= pow(2,self.data_bitwidth)):
            raise cocotb.result.TestError("Settings Bus Write: Data out of bounce")
        if(address >= pow(2,self.address_bitwidth)):
            raise cocotb.result.TestError("Settings Bus Write: Address out of bounce")

        self.bus.set_data <= data
        self.bus.set_addr <= address
        self.bus.set_stb  <= 1
        
        yield RisingEdge(self.clock)
        self.bus.set_stb <= 0

    @cocotb.coroutine
    def read(self,address:int) -> int:
        yield RisingEdge(self.clock)

        if(address >= pow(2,self.address_bitwidth)):
            raise cocotb.result.TestError("Settings Bus Read: Address out of bounce")

        self.bus.set_addr <= address
        yield RisingEdge(self.clock)
        data = self.bus.rb_data.value.get_value()

        if(data >= pow(2,self.data_bitwidth)):
            raise cocotb.result.TestError("Settings Bus Read: Data out of bounce")

        return data



class SettingsBusMonitor(BusMonitor):
    _signals = {"set_addr":"addr_i", "set_stb":"strobe_i", "set_data":"data_i", "rb_data":"rbdata_o"}
    _optional_signals = {"rb_strobe":"rbstrobe_o"}

    def __init___(self,entity: cocotb.handle.HierarchyObject, name: str, clock:cocotb.handle.ModifiableObject,data_bitwidth:int,address_bitwidth:int,**kwargs):
        BusMonitor.__init__(self,entity,name,clock,**kwargs)

        self.data_bitwidth=data_bitwidth
        self.address_bitwidth = address_bitwidth

        self.log.debug("SettingsBusMonitor created")


    @cocotb.coroutine
    def _monitor_recv(self):

        while True:
            yield RisingEdge(self.clock)
            yield ReadOnly()

            #if(data >= pow(2,self.data_bitwidth)):
            #    raise cocotb.result.TestError("Settings Bus Write: Data out of bounce")
            #if(address >= pow(2,self.address_bitwidth)):
            #    raise cocotb.result.TestError("Settings Bus Write: Address out of bounce")
            
            if(self.bus.set_stb==1):
                address = self.bus.set_addr.value
                data = self.bus.set_data.value
                self._recv({"address":address, "data":data})
        
                
        
