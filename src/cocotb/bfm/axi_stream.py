import cocotb
from cocotb.drivers import BusDriver
from cocotb.monitors import BusMonitor
from cocotb.decorators import coroutine
from cocotb.triggers import RisingEdge, FallingEdge, ReadOnly, NextTimeStep, Event
from cocotb.result import TestError



class AXIStreamMaster(BusDriver):
    _signals = {"TDATA": "tdata_i", "TREADY":"tready_i","TVALID":"tvalid_i","TLAST":"tlast_i"}

    def __init__(self,entity: cocotb.handle.HierarchyObject, name: str, clock:cocotb.handle.ModifiableObject,data_bitwidth:int,address_bitwidth:int, signal_prefix:str="i",**kwargs):

        self._signals = {"TDATA": signal_prefix + "_tdata", "TREADY": signal_prefix + "_tready","TVALID": signal_prefix + "_tvalid","TLAST": signal_prefix + "_tlast"}
        
        BusDriver.__init__(self,entity,name,clock,**kwargs)
        
        self.data_bitwidth=data_bitwidth
        self.address_bitwidth = address_bitwidth

        # drive defaults
        self.bus.TDATA.setimmediatevalue(0)
        self.bus.TVALID.setimmediatevalue(0)
        self.bus.TLAST.setimmediatevalue(0)

        self.log.debug("AXIStreamMaster created")

    @cocotb.coroutine
    def reset(self, sync = True):
        if sync:
            yield RisingEdge(self.clock)

        self.bus.TDATA    <= 0
        self.bus.TVALID   <= 0
        self.bus.TLAST    <= 0


    @cocotb.coroutine
    def write(self, tdata):
        
        for i,data in enumerate(tdata):
            self.bus.TDATA     <= data
            self.bus.TVALID    <= 1
    
            if i == len(tdata)-1:
                self.bus.TLAST <= 1

            while True:
                yield RisingEdge(self.clock)
                if not hasattr(self.bus,"TREADY") or int(self.bus.TREADY) == 1:
                    break
    
            
        self.bus.TVALID   <= 0
        self.bus.TLAST    <= 0

class AXIConfMaster(BusDriver):
    _signals = {"TDATA": "tdata_conf", "TREADY":"tready_conf","TVALID":"tvalid_conf","TLAST":"tlast_conf"}

    def __init__(self,entity: cocotb.handle.HierarchyObject, name: str, clock:cocotb.handle.ModifiableObject,data_bitwidth:int,address_bitwidth:int,**kwargs):
        BusDriver.__init__(self,entity,name,clock,**kwargs)
        
        self.data_bitwidth=data_bitwidth
        self.address_bitwidth = address_bitwidth

        # drive defaults
        self.bus.TDATA.setimmediatevalue(0)
        self.bus.TVALID.setimmediatevalue(0)
        self.bus.TLAST.setimmediatevalue(0)

        self.log.debug("AXIConfMaster created")

    @cocotb.coroutine
    def reset(self, sync = True):
        if sync:
            yield RisingEdge(self.clock)

        self.bus.TDATA    <= 0
        self.bus.TVALID   <= 0
        self.bus.TLAST    <= 0


    @cocotb.coroutine
    def write(self, tdata):
        
        for i,data in enumerate(tdata):
            self.bus.TDATA     <= data
            self.bus.TVALID    <= 1
    
            if i == len(tdata)-1:
                self.bus.TLAST <= 1

            while True:
                yield RisingEdge(self.clock)
                if not hasattr(self.bus,"TREADY") or int(self.bus.TREADY) == 1:
                    break
    
            
        self.bus.TVALID   <= 0
        self.bus.TLAST    <= 0

    
class AXIStreamMonitor(BusMonitor):
    _signals = {"TDATA": "tdata_o", "TREADY":"tready_o","TVALID":"tvalid_o","TLAST":"tlast_o"}

    def __init__(self,entity: cocotb.handle.HierarchyObject, name: str, clock:cocotb.handle.ModifiableObject, \
                 data_bitwidth:int,address_bitwidth:int,isInput:bool=False,signal_prefix:str="i0",**kwargs):
        
        #if(isInput):
        self._signals = {"TDATA": signal_prefix + "_tdata", "TREADY": signal_prefix + "_tready","TVALID": signal_prefix + "_tvalid","TLAST": signal_prefix + "_tlast"}

        BusMonitor.__init__(self,entity,name,clock,**kwargs)

        self.data_bitwidth=data_bitwidth
        self.address_bitwidth = address_bitwidth

        self.log.debug("AXIStreamMonitor created")


    @cocotb.coroutine
    def _monitor_recv(self):

        tdata = []
        while True:
            yield RisingEdge(self.clock)
            yield ReadOnly()
            
            if (not hasattr(self.bus,"TREADY") or int(self.bus.TREADY) == 1) and int(self.bus.TVALID):
                tdata.append(self.bus.TDATA.value)

                if int(self.bus.TLAST):
                    self._recv(tdata)
                    tdata=[]
                    

class AXIConfMonitor(BusMonitor):
    _signals = {"TDATA": "tdata_conf", "TREADY":"tready_conf","TVALID":"tvalid_conf","TLAST":"tlast_conf"}

    def __init__(self,entity: cocotb.handle.HierarchyObject, name: str, clock:cocotb.handle.ModifiableObject, \
                 data_bitwidth:int,address_bitwidth:int,isInput:bool=False,**kwargs):
        
        if(isInput):
            self._signals = {"TDATA": "tdata_conf", "TREADY":"tready_conf","TVALID":"tvalid_conf","TLAST":"tlast_conf"}

        BusMonitor.__init__(self,entity,name,clock,**kwargs)

        self.data_bitwidth=data_bitwidth
        self.address_bitwidth = address_bitwidth

        self.log.debug("AXIConfMonitor created")


    @cocotb.coroutine
    def _monitor_recv(self):

        tdata = []
        while True:
            yield RisingEdge(self.clock)
            yield ReadOnly()
            
            if (not hasattr(self.bus,"TREADY") or int(self.bus.TREADY) == 1) and int(self.bus.TVALID):
                tdata.append(self.bus.TDATA.value)

                if int(self.bus.TLAST):
                    self._recv(tdata)
                    tdata=[]
                    
