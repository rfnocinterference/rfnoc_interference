-- RFNoC interference (c) by David Haberleitner
--
-- RFNoC interference is licensed under a
-- Creative Commons Attribution-ShareAlike 4.0 International License.
--
-- You should have received a copy of the license along with this
-- work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fadewin_impl is
  port (
    i_clk : in std_ulogic;
    i_rst : in std_ulogic;

    -- input interface
    i_tdata  : in  std_ulogic_vector(31 downto 0);
    i_tready : out std_ulogic;
    i_tvalid : in  std_ulogic;
    i_tlast  : in  std_ulogic;

    -- output interface
    o_tdata  : out std_ulogic_vector(31 downto 0);
    o_tready : in  std_ulogic;
    o_tvalid : out std_ulogic;
    o_tlast  : out std_ulogic;

    -- config interface coefficients
    conf_window_tdata  : in  std_ulogic_vector(15 downto 0);
    conf_window_tready : out std_ulogic;
    conf_window_tvalid : in  std_ulogic;
    conf_window_tlast  : in  std_ulogic;

    -- sets the ouput vector size: needed for streaming samplesback to the host
    -- (MTU limit)
    spp_out : in std_ulogic_vector(15 downto 0)
    );
end entity fadewin_impl;

architecture rtl of fadewin_impl is

  type t_state is (consume);

  type complex_16 is record  -- complex type for storing real and imag part
    re : std_ulogic_vector(15 downto 0);
    im : std_ulogic_vector(15 downto 0);
  end record complex_16;

  constant complex_16_zero : complex_16 := (
    re => (others => '0'),
    im => (others => '0')
    );

  type reg_set_t is record
    state : t_state;

    sample_idx   : unsigned(9 downto 0);
    sample_in1_r : complex_16;
    sample_in2_r : complex_16;

    oldvec_out_r     : complex_16;
    mult_new_out_r   : complex_16;
    mult_old_out_r   : complex_16;
    window_out_r     : std_ulogic_vector(15 downto 0);
    window_out_inv_r : std_ulogic_vector(15 downto 0);
    sample_out_r     : complex_16;


    valid_pipe : std_ulogic_vector(8 downto 0);

    samps_out : unsigned(15 downto 0);
  end record;



  constant reset_val_c : reg_set_t := (
    state            => consume,
    sample_idx       => (others => '0'),
    sample_in1_r     => complex_16_zero,
    sample_in2_r     => complex_16_zero,
    oldvec_out_r     => complex_16_zero,
    mult_new_out_r   => complex_16_zero,
    mult_old_out_r   => complex_16_zero,
    window_out_r     => (others => '0'),
    window_out_inv_r => (others => '0'),
    sample_out_r     => complex_16_zero,

    valid_pipe => (others => '0'),
    samps_out  => (others => '0')
    );

  signal reg, nxt_reg : reg_set_t;

  --signal oldvec_raddr : std_ulogic_vector(9 downto 0);
  signal oldvec_waddr : std_ulogic_vector(9 downto 0);
  signal oldvec_in    : std_ulogic_vector(31 downto 0);
  signal oldvec_out   : std_ulogic_vector(31 downto 0);


  -- signal win_raddr : std_ulogic_vector(9 downto 0);
  -- signal win_waddr : std_ulogic_vector(9 downto 0);
  signal win_out   : std_ulogic_vector(15 downto 0);
  signal win_waddr : std_ulogic_vector(9 downto 0);

  signal mult_new_out : complex_16;
  signal mult_old_out : complex_16;

  -- maximum value for a 16 bit sample
  constant signed16_max_c : natural := (2**(16-1)-1);

begin  -- architecture rtl

  oldvec_waddr <= std_ulogic_vector(reg.sample_idx - 1);
  oldvec_in    <= reg.sample_in1_r.im & reg.sample_in1_r.re;

  ram_oldvec : entity work.ram_2p
    generic map (
      g_width      => 32,
      g_addr_width => 10,
      g_length     => 1024)
    port map (
      clk   => i_clk,
      re    => o_tready,
      we    => reg.valid_pipe(0),
      raddr => std_ulogic_vector(reg.sample_idx),
      waddr => oldvec_waddr,
      d     => oldvec_in,
      o     => oldvec_out);

  ram_win : entity work.ram_2p
    generic map (
      g_width      => 16,
      g_addr_width => 10,
      g_length     => 1024)
    port map (
      clk   => i_clk,
      re    => o_tready,
      we    => conf_window_tvalid,
      raddr => std_ulogic_vector(reg.sample_idx),
      waddr => win_waddr,
      d     => conf_window_tdata,
      o     => win_out);

  -- multipliers
  -- nxt_reg.mult_new_out_r.im <= reg.sample_in2_r.im * reg.window_out_r;
  mult_16x16_1 : entity work.mult_16x16
    generic map (
      g_input_width  => 16,
      g_output_width => 16)
    port map (
      clk => i_clk,
      rst => i_rst,
      ena => o_tready,
      Ain => reg.sample_in2_r.im,
      Bin => reg.window_out_r,
      P   => mult_new_out.im);

  mult_16x16_2 : entity work.mult_16x16
    generic map (
      g_input_width  => 16,
      g_output_width => 16)
    port map (
      clk => i_clk,
      rst => i_rst,
      ena => o_tready,
      Ain => reg.sample_in2_r.re,
      Bin => reg.window_out_r,
      P   => mult_new_out.re);

  mult_16x16_3 : entity work.mult_16x16
    generic map (
      g_input_width  => 16,
      g_output_width => 16)
    port map (
      clk => i_clk,
      rst => i_rst,
      ena => o_tready,
      Ain => reg.oldvec_out_r.im,
      Bin => reg.window_out_inv_r,
      P   => mult_old_out.im);

  mult_16x16_4 : entity work.mult_16x16
    generic map (
      g_input_width  => 16,
      g_output_width => 16)
    port map (
      clk => i_clk,
      rst => i_rst,
      ena => o_tready,
      Ain => reg.oldvec_out_r.re,
      Bin => reg.window_out_inv_r,
      P   => mult_old_out.re);


  -- purpose: write window coefficients from config bus to the internal ram
  -- type   : sequential
  -- inputs : i_clk, i_rst, conf_window_tvalid
  -- outputs: win_waddr
  write_window_coeffs : process (i_clk, i_rst) is
  begin  -- process write_window_coeffs
    if i_rst = '1' then                     -- asynchronous reset (active high)
      win_waddr          <= (others => '0');
      conf_window_tready <= '1';
    elsif i_clk'event and i_clk = '1' then  -- rising clock edge
      if conf_window_tvalid = '1' then
        win_waddr <= std_ulogic_vector(unsigned(win_waddr) + 1);
      end if;
      -- always ready for data
      conf_window_tready <= '1';
    end if;
  end process write_window_coeffs;

  -- purpose: register process
  -- type   : sequential
  -- inputs : i_clk, i_rst
  -- outputs: 
  reg_proc : process (i_clk, i_rst) is
  begin  -- process reg_proc

    if i_clk'event and i_clk = '1' then  -- rising clock edge
      if i_rst = '1' then                -- asynchronous reset (active low)
        reg <= reset_val_c;
      else
        reg <= nxt_reg;
      end if;
    end if;
  end process reg_proc;

  -- output value from last register stage
  o_tdata  <= reg.sample_out_r.im & reg.sample_out_r.re;
  -- directly connect input and output ready signals
  i_tready <= o_tready;


  -- purpose: state machine transistions
  -- type   : combinational
  comb : process (reg, o_tready, i_tdata, i_tvalid, i_tlast, win_out, oldvec_out, mult_new_out, mult_old_out) is
  begin  -- process comb
    -- default assignments
    nxt_reg <= reg;
    o_tlast <= '0';

    -- directly output valid from pipe
    o_tvalid <= reg.valid_pipe(6);


    case reg.state is
      when consume =>

        -- stop pipeline if downstream is not ready
        if o_tready = '1' then

          nxt_reg.valid_pipe(6 downto 1) <= reg.valid_pipe(5 downto 0);
          nxt_reg.valid_pipe(0)          <= '0';

          if reg.valid_pipe(6) = '1' then
            nxt_reg.samps_out <= reg.samps_out + 1;

            if reg.samps_out = unsigned(spp_out)-1 then
              o_tlast           <= '1';
              nxt_reg.samps_out <= (others => '0');
            end if;
          end if;



          -- consume sample from input
          if i_tvalid = '1' then
            nxt_reg.valid_pipe(0) <= i_tvalid;
            nxt_reg.sample_idx    <= reg.sample_idx + 1;

          end if;


          nxt_reg.sample_in1_r.im <= i_tdata(31 downto 16);
          nxt_reg.sample_in1_r.re <= i_tdata(15 downto 0);
          nxt_reg.sample_in2_r <= reg.sample_in1_r;          

          nxt_reg.window_out_r     <= win_out;
          nxt_reg.window_out_inv_r <= std_ulogic_vector(to_signed(signed16_max_c, nxt_reg.window_out_inv_r'length) - signed(win_out));

          nxt_reg.oldvec_out_r.im <= oldvec_out(31 downto 16);
          nxt_reg.oldvec_out_r.re <= oldvec_out(15 downto 0);

          nxt_reg.mult_new_out_r <= mult_new_out;
          nxt_reg.mult_old_out_r <= mult_old_out;

          nxt_reg.sample_out_r.im <= std_ulogic_vector(resize(signed(reg.mult_new_out_r.im) + signed(reg.mult_old_out_r.im), nxt_reg.sample_out_r.im'length));
          nxt_reg.sample_out_r.re <= std_ulogic_vector(resize(signed(reg.mult_new_out_r.re) + signed(reg.mult_old_out_r.re), nxt_reg.sample_out_r.re'length));

        end if;

      when others => null;
    end case;
  end process comb;


end architecture rtl;
