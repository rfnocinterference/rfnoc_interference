
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter
from math import e, pi
import random

import cocotb
from cocotb.binary import BinaryValue
from cocotb.triggers import Timer, RisingEdge, ReadOnly
from cocotb.clock import Clock
from fadewin_tb import FadewinTB


@cocotb.test()
def main_test(dut):
    dut._log.info("######### Start of test sequence #########")
    samps_per_vec = int(1024)
    vecs = 10
    
    clk_1mhz   = Clock(dut.i_clk, 1.0, units='us')
    clk_gen = cocotb.fork(clk_1mhz.start())

    dut.o_tready <= 0
    dut.spp_out <= 256;
    
    # insantiate testbench and reset
    tb = FadewinTB(dut)
    yield tb.reset()

    spec = np.zeros((samps_per_vec), dtype=complex)
    spec[100] = 1000
    transformed = np.fft.ifft(spec)

    
    in_signal_re = (transformed.real * (2**15 - 1)).astype(int)
    in_signal_im = (transformed.imag * (2**15 - 1)).astype(int)

    in_signal = (in_signal_re & 0xFFFF) | ((in_signal_im & 0xFFFF) << 16)
    # in_signal = np.ones((samps_per_vec),dtype=int)*(2**15 - 1)
    # in_signal = (np.linspace(0,1,num=samps_per_vec)*(2**15 - 1)).astype(int)

    yield Timer(20,"us")

    window = np.hanning(1024*2+1)[0:1024] * (2**15 - 1)
    print(window.shape)

    yield tb.stream_win_config.write(window.astype(int).tolist())


    dut.o_tready <= 1
    
    # write some values

    yield tb.stream_config.write(np.zeros((samps_per_vec),dtype=int).tolist())
    yield Timer(100.5,"us")
    cocotb.fork(tb.stream_config.write(in_signal.tolist()))
    yield Timer(100,"us")
    dut.o_tready <= 0
    yield Timer(200,"us")
    dut.o_tready <= 1
    yield Timer(2,"ms")

    for i in range(0,vecs):
        yield tb.stream_config.write(in_signal.tolist())

    yield Timer(100,"us")
    dut.o_tready <= 0

    dut._log.info("######### End of test sequence #########")

    sig_in = np.array(tb.get_in_signal())
    sig_out = np.array(tb.get_out_signal())

    fs = 200e6

    x = sig_out[2*samps_per_vec:]
    t = np.linspace(0,len(x)*(1/fs),len(x))
    x_win = x * np.hanning(len(x))
    X = 10 * np.log10(np.abs(np.fft.fftshift(np.fft.fft(x_win)/len(x_win))**2))
    f = np.fft.fftshift(np.fft.fftfreq(len(X), (1/fs)))
    form_hz = EngFormatter(unit='Hz')
    form_s = EngFormatter(unit='s')

    plt.figure()
    plt.title("Input signal")
    plt.plot(sig_in.real)
    plt.plot(sig_in.imag)

    plt.figure()
    plt.title("Output signal")
    plt.plot(sig_out.real)
    plt.plot(sig_out.imag)

    fig3 = plt.figure()
    ax3 = fig3.add_subplot(111)
    ax3.set_title("spectrum")
    ax3.plot(f, X)
    plt.xlabel("frequency")
    plt.ylabel("power [dBFS]")
    ax3.xaxis.set_major_formatter(form_hz)

    plt.show()

    
