import cocotb
from cocotb.binary import BinaryValue
from cocotb.triggers import Timer, RisingEdge, ReadOnly
from cocotb.scoreboard import Scoreboard
import sys
import pathlib
import numpy as np
import logging

# add path of busfunctional model
sys.path.append(str(pathlib.Path(__file__).parent.absolute()) + "/../../../cocotb")
from bfm import SettingsBusMaster,SettingsBusMonitor
from bfm import AXIStreamMaster, AXIStreamMonitor
from bfm import AXIConfMaster, AXIConfMonitor

class FadewinModel(object):

    def __init__(self):
        self.expected_output = []

    def settings_bus_operation(self,transaction):
        pass


    def calculate(self,transaction:list):
        """
        Simple fadewin block model (binary representation)
        """

        i=0
        for item in transaction:
            x = item[16:31].signed_integer / (2**15)
            y = item[0:15].signed_integer / (2**15)
            z = x + y*1j
            print("Input {}: {}, {}, abs: {}".format(i,x,y,abs(z)))
            i = i+1

            self.in_signal.append(z)



class FadewinTB(object):
    def __init__(self,dut: cocotb.handle.HierarchyObject):
        self.in_signal = []
        self.out_signal = []
        
        self.dut = dut
        self.dut._log.debug("Building/Connecting Testbench")

        #self.model = FadewinModel()
        
        # must connect recovermonitor before stream_out monitor, else error in scoreboard because model would not get called before 
        # hardware has done the calculation
        self.recoverStream = AXIStreamMonitor(dut,None,dut.i_clk,32,32,isInput=True,callback=self.input_callback,signal_prefix="i")
        self.stream_config = AXIStreamMaster(dut,None,dut.i_clk,32,32,signal_prefix="i")

        self.recover_win_config = AXIStreamMonitor(dut,None,dut.i_clk,16,16,isInput=True,signal_prefix="conf_window")
        self.stream_win_config = AXIStreamMaster(dut,None,dut.i_clk,16,16,signal_prefix="conf_window")

        self.stream_out = AXIStreamMonitor(dut,None,dut.i_clk,32,32,callback=self.output_callback,signal_prefix="o")
        #self.settingsMonitor   = SettingsBusMonitor(dut,"sb",dut.i_clk,32,8,callback=self.model.settings_bus_operation)

        self.scoreboard = Scoreboard(dut)
        #self.scoreboard.add_interface(self.stream_out,self.model.expected_output)


    def input_callback(self, transaction:list):
        print("Got transaction with {} values".format(len(transaction)))
        i=0
        for item in transaction:
            x = item[16:31].signed_integer / (2**15)
            y = item[0:15].signed_integer / (2**15)
            z = x + y*1j
            print("Input {}: {}, {}, abs: {}".format(i,x,y,abs(z)))
            i = i+1

            self.in_signal.append(z)

    def output_callback(self,transaction:list):
        """
        Simple fadewin block model (binary representation)
        """

        i=0
        for item in transaction:
            x = item[16:31].signed_integer / (2**15)
            y = item[0:15].signed_integer / (2**15)
            z = x + y*1j
            print("Output {}: {}, {}, abs: {}".format(i,x,y,abs(z)))
            i = i+1

            self.out_signal.append(z)


    def get_in_signal(self):
        return self.in_signal

    def get_out_signal(self):
        return self.out_signal


    @cocotb.coroutine
    async def reset(self):
        
        self.dut.i_rst <= 0
        await RisingEdge(self.dut.i_clk)
        await self.stream_config.reset()
        #await self.settings.reset()

        self.dut.i_rst <= 1
        await Timer(100,units="ns")
        await RisingEdge(self.dut.i_clk)
        self.dut.i_rst <= 0
        await RisingEdge(self.dut.i_clk)
