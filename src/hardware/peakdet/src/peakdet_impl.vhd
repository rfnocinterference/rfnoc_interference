-- RFNoC interference (c) by David Haberleitner
--
-- RFNoC interference is licensed under a
-- Creative Commons Attribution-ShareAlike 4.0 International License.
--
-- You should have received a copy of the license along with this
-- work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity peakdet_impl is
  port (
    i_clk : in std_ulogic;
    i_rst : in std_ulogic;


    -- output interface
    o_tdata  : out std_ulogic_vector(15 downto 0);
    o_tready : in  std_ulogic;
    o_tvalid : out std_ulogic;
    o_tlast  : out std_ulogic;

    -- config interface coefficients
    i_tdata  : in  std_ulogic_vector(15 downto 0);
    i_tready : out std_ulogic;
    i_tvalid : in  std_ulogic;
    i_tlast  : in  std_ulogic;

    i_decimation : in std_ulogic_vector(15 downto 0)
    );
end entity peakdet_impl;

architecture rtl of peakdet_impl is
  type t_state is (fill_ram_fifo, compare, send);

  type reg_set_t is record
    state                   : t_state;
    sample_idx_out          : unsigned(9 downto 0);
    sample_idx_in           : unsigned(9 downto 0);

    ram_idx                 : unsigned(9 downto 0);

    iteration : unsigned(15 downto 0);
    r_i_tready : std_ulogic;
    
    r_o_tvalid : std_ulogic;
    r_o_tlast : std_ulogic;
    -- one bit 
    -- field_idx_out           : unsigned(0 downto 0);
    -- field_idx_in            : unsigned(0 downto 0);

  end record;

  constant reset_val_c : reg_set_t := (
    state                   => fill_ram_fifo,
    sample_idx_out          => (others => '0'),
    sample_idx_in           => (others => '0'),
    ram_idx => (others => '0'),
    iteration => (others => '0'),

    r_i_tready => '0',

    r_o_tvalid => '0',
    r_o_tlast => '0'
    );

  signal reg, nxt_reg : reg_set_t;

  signal ram_re : std_ulogic;
  signal ram_we : std_ulogic;

  signal ram_raddr : std_ulogic_vector(9 downto 0);
  signal ram_waddr : std_ulogic_vector(9 downto 0);

  signal ram_i_data : std_ulogic_vector(15 downto 0);
  signal ram_o_data : std_ulogic_vector(15 downto 0);

  

begin  -- architecture rtl

  ram_raddr <= std_ulogic_vector(reg.ram_idx);
  ram_waddr <= std_ulogic_vector(reg.sample_idx_in);

  rams_2p_1 : entity work.ram_2p
    generic map (
      g_width      => 16,
      g_addr_width => 10,
      g_length     => 1024)
    port map (
      clk   => i_clk,
      we    => ram_we,
      re    => ram_re,
      raddr => ram_raddr,
      waddr => ram_waddr,
      d     => ram_i_data,
      o     => ram_o_data);


  -- purpose: register process
  -- type   : sequential
  -- inputs : i_clk, i_rst
  -- outputs: 
  reg_proc : process (i_clk, i_rst) is
  begin  -- process reg_proc
    if i_clk'event and i_clk = '1' then  -- rising clock edge
      if i_rst = '1' then                -- synchronous reset (active high)
        reg <= reset_val_c;
      else
        reg <= nxt_reg;
      end if;
    end if;
  end process reg_proc;

  -- purpose: state machine transistions
  -- type   : combinational
  -- inputs : reg
  -- outputs: 
  comb : process (reg, i_tdata, i_tvalid, i_tlast) is
  begin  -- process comb

    nxt_reg <= reg;
    
    -- default assignments
    ram_re <= '0';
    ram_we <= '0';

    ram_i_data <= i_tdata;
    o_tdata <= ram_o_data;

    o_tvalid <= reg.r_o_tvalid;
    o_tlast <= reg.r_o_tlast;
    i_tready <= reg.r_i_tready;

    nxt_reg.r_o_tvalid <= '0';
    nxt_reg.r_o_tlast <= '0';

    case reg.state is
      when fill_ram_fifo =>
        ram_re <= '1';
        nxt_reg.state <= compare;
        nxt_reg.ram_idx <= to_unsigned(1, nxt_reg.ram_idx'length);

        -- start consuming samples next cycle
        nxt_reg.r_i_tready <= '1';
        
      when compare =>
        if (i_tvalid = '1') then
          ram_re <= '1';

          -- write new value if it is larger than the old one
          if unsigned(i_tdata) > unsigned(ram_o_data) then
            ram_we <= '1';
          end if;

          -- increase counters
          nxt_reg.ram_idx <= reg.ram_idx + 1;
          nxt_reg.sample_idx_in <= reg.sample_idx_in + 1;

          if reg.sample_idx_in = 1024-1 then
            nxt_reg.state <= fill_ram_fifo;
            nxt_reg.r_i_tready <= '0';
            nxt_reg.ram_idx <= (others => '0');
            nxt_reg.iteration <= reg.iteration + 1;

            -- move to next 
            if reg.iteration = unsigned(i_decimation) then
              nxt_reg.iteration <= (others => '0');
              nxt_reg.state <= send;
              nxt_reg.r_i_tready <= '0';
            end if;
          end if;
        end if;

      when send =>
        if o_tready = '1' then

          -- read values one by one
          ram_re <= '1';
          nxt_reg.ram_idx <= reg.ram_idx + 1;
          
          -- feed valid into pipe
          nxt_reg.r_o_tvalid <= '1';

          -- clear complete memory
          ram_i_data <= (others => '0');
          ram_we <= '1';
          nxt_reg.sample_idx_in <= reg.sample_idx_in + 1;

          -- limit packet size by asserting tlast
          if reg.sample_idx_in(7 downto 0) = 256-1 then
            nxt_reg.r_o_tlast <= '1';
          end if;
          
          if reg.sample_idx_in = 1024-1 then
            nxt_reg.state <= fill_ram_fifo;
            nxt_reg.r_i_tready <= '0';
          end if;
        end if;
      when others => null;
    end case;
  end process comb;

end architecture rtl;
