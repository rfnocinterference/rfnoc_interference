import cocotb
from cocotb.binary import BinaryValue
from cocotb.triggers import Timer, RisingEdge, ReadOnly
from cocotb.scoreboard import Scoreboard
import sys
import pathlib
import numpy as np
import logging

# add path of busfunctional model
sys.path.append(str(pathlib.Path(__file__).parent.absolute()) + "/../../../cocotb")
from bfm import AXIStreamMaster, AXIStreamMonitor

class PeakdetModel(object):
    ADDRESS_GAIN = 0
    ADDRESS_COUNTER = 1

    def __init__(self):
        self.expected_output = []


    def settings_bus_operation(self,transaction):
        pass


    def calculate(self,transaction:list):
        """
        Simple peakdet block model (binary representation)
        """

        # duration = transaction.pop().signed_integer

        for i in range(0, len(transaction)):
            print("Sent: {}: {}".format(i, transaction[i].signed_integer))

        # for i in range(0,duration):
        #     self.expected_output.append(transaction)



class PeakdetTB(object):
    def __init__(self,dut: cocotb.handle.HierarchyObject):
        self.dut = dut
        self.dut._log.debug("Building/Connecting Testbench")

        self.model = PeakdetModel()
        
        # must connect recovermonitor before stream_out monitor, else error in scoreboard because model would not get called before 
        # hardware has done the calculation
        self.recoverStream = AXIStreamMonitor(dut,None,dut.i_clk,16,16,isInput=True,callback=self.model.calculate,signal_prefix="i")
        self.stream_in = AXIStreamMaster(dut,None,dut.i_clk,16,16,signal_prefix="i")

        self.stream_out = AXIStreamMonitor(dut,None,dut.i_clk,16,16,callback=self.printRes,signal_prefix="o")

        self.scoreboard = Scoreboard(dut)
        #self.scoreboard.add_interface(self.stream_out,self.model.expected_output)


    def printRes(self, transaction:list):
        print("Got transaction with {} values".format(len(transaction)))
        i=0
        for item in transaction:

            print("Received {}: {}".format(i,item.signed_integer))
            i = i+1

    @cocotb.coroutine
    async def reset(self):
        
        self.dut.i_rst <= 0
        await RisingEdge(self.dut.i_clk)
        await self.stream_in.reset()
        #await self.settings.reset()

        self.dut.i_rst <= 1
        await Timer(100,units="ns")
        await RisingEdge(self.dut.i_clk)
        self.dut.i_rst <= 0
        await RisingEdge(self.dut.i_clk)
