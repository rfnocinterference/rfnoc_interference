
import numpy as np
import matplotlib.pyplot as plt
from math import e, pi
import random

import cocotb
from cocotb.binary import BinaryValue
from cocotb.triggers import Timer, RisingEdge, ReadOnly
from cocotb.clock import Clock
from peakdet_tb import PeakdetTB


@cocotb.test()
def main_test(dut):
    dut._log.info("######### Start of test sequence #########")
    
    clk_1mhz   = Clock(dut.i_clk, 1.0, units='us')
    clk_gen = cocotb.fork(clk_1mhz.start())

    dut.o_tready <= 0

    # 1:1 "downsampling"
    dut.i_decimation <= 1
    
    # insantiate testbench and reset
    tb = PeakdetTB(dut)
    yield tb.reset()

    yield Timer(5.5,"us")

    dut.o_tready <= 1

    #in_signal = np.ones(1024, dtype=int) * 50
    in_signal = np.linspace(1024, 0, 1024, dtype=int)
    yield tb.stream_in.write(in_signal.tolist())
    
    yield Timer(5.5,"us")
    
    in_signal = np.linspace(0, 1024, 1024, dtype=int)
    yield tb.stream_in.write(in_signal.tolist())


    #in_signal = np.linspace(0, 1, 1024, dtype=int)
    #yield tb.stream_in.write(in_signal.tolist())

    
    #in_signal = np.linspace(0, 0, 1024, dtype=int)
    #yield tb.stream_in.write(in_signal.tolist())


    # dut.o_tready <= 1

    #yield Timer(1.5,"ms")

    in_signal = np.linspace(0, 1, 1024, dtype=int)
    yield tb.stream_in.write(in_signal.tolist())

    #yield Timer(5.1,"us")
    
    in_signal = np.linspace(0, 0, 1024, dtype=int)
    yield tb.stream_in.write(in_signal.tolist())

    yield Timer(1.5,"ms")
    
    dut.o_tready <= 0



    # yield Timer(5,"ms")
    # dut.o_tready <= 0

    # plt.figure()
    # plt.plot(out_signal)
    # plt.show()

    dut._log.info("######### End of test sequence #########")
