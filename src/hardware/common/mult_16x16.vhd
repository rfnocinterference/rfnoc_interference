-- RFNoC interference (c) by David Haberleitner
--
-- RFNoC interference is licensed under a
-- Creative Commons Attribution-ShareAlike 4.0 International License.
--
-- You should have received a copy of the license along with this
-- work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.


library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- use IEEE.STD_LOGIC_SIGNED.all;
use ieee.numeric_std.all;

entity mult_16x16 is
  generic (
    g_input_width  : natural := 16;
    g_output_width : natural := 16);
  port (clk      : in  std_ulogic;
        rst      : in  std_ulogic;
        ena      : in  std_ulogic;
        Ain, Bin : in  std_ulogic_vector((g_input_width-1) downto 0);
        P        : out std_ulogic_vector((g_output_width-1) downto 0));
end mult_16x16;

architecture Behavioral of mult_16x16 is
  constant ZERO              : signed(47 downto 0) := (others => '0');
  signal AR, BR              : signed(17 downto 0);
  signal MULTR               : signed(35 downto 0);
  signal Pout                : signed(47 downto 0);
  signal Carry_in, Carry_inR : signed(0 downto 0);
begin
  process(clk)
  begin
    if rising_edge(clk) then
      Carry_in(0) <= not(Ain(g_input_width-1) xor Bin(g_input_width-1));
      Carry_inR   <= Carry_in;
      if rst = '1' then
        AR    <= (others => '0');
        BR    <= (others => '0');
        MULTR <= (others => '0');
        Pout  <= (others => '0');
      else
        if ena = '1' then
          -- sign extend the inputs to 18 bit
          AR                             <= (others => Ain(g_input_width-1));
          BR                             <= (others => Bin(g_input_width-1));
          AR(17 downto (18-(g_input_width))) <= signed(Ain);
          BR(17 downto (18-(g_input_width))) <= signed(Bin);

          MULTR <= AR * BR;

          -- Note that the following 4 operands adder will be
          -- implemented as a 3 operand one : ZERO is a constant that
          -- allows easy sign extension for the VHDL syntax
          Pout <= ZERO + MULTR; -- + x"1FFFF" + Carry_inR;
        end if;

      end if;
    end if;
  end process;
  P <= std_ulogic_vector(Pout(34 downto 19));
end Behavioral;
