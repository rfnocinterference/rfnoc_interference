-- RFNoC interference (c) by David Haberleitner
--
-- RFNoC interference is licensed under a
-- Creative Commons Attribution-ShareAlike 4.0 International License.
--
-- You should have received a copy of the license along with this
-- work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_pkg.all;
-- use work.fixed_pkg.all;


entity complex_mult is
  
  port (
    i_clk : in std_ulogic;
    i_rst : in std_ulogic;
    i_ena : in std_ulogic;
    i_a : in std_ulogic_vector(31 downto 0);
    i_b : in std_ulogic_vector(31 downto 0);
    o_z : out std_ulogic_vector(31 downto 0)
    );
  
end entity complex_mult;

architecture rtl of complex_mult is

  type cfixed_16 is record  -- complex type for storing real and imag part
    re : sfixed(0 downto -15);
    im : sfixed(0 downto -15);
  end record cfixed_16;

  constant cfixed_16_zero : cfixed_16 := (
    re => (others => '0'),
    im => (others => '0')
    );



  signal input_reg_a : cfixed_16;
  signal input_reg_b : cfixed_16;

  signal reg_re1 : sfixed(1 downto -30);
  signal reg_re2 : sfixed(1 downto -30);

  signal reg_im1 : sfixed(1 downto -30);
  signal reg_im2 : sfixed(1 downto -30);

  signal reg2_re1 : sfixed(0 downto -15);
  signal reg2_re2 : sfixed(0 downto -15);

  signal reg2_im1 : sfixed(0 downto -15);
  signal reg2_im2 : sfixed(0 downto -15);

  
begin  -- architecture rtl

  
  -- purpose: multiplication of a axi streaming data channel with an always valid coefficient
  -- type   : sequential
  -- inputs : i_clk, i_rst
  -- outputs: o_tdata
  process (i_clk, i_rst) is
    variable o_z_real : sfixed(0 downto -15);
    variable o_z_imag : sfixed(0 downto -15);


  begin  -- process
    if i_clk'event and i_clk = '1' then  -- rising clock edge
      if i_rst = '1' then                 -- asynchronous reset (active low)
        o_z <= (others => '0');
        input_reg_a <= cfixed_16_zero;
        input_reg_b <= cfixed_16_zero;

        reg_re1 <= (others => '0');
        reg_re2 <= (others => '0');
        reg_im1 <= (others => '0');
        reg_im2 <= (others => '0');
        reg2_re1 <= (others => '0');
        reg2_re2 <= (others => '0');
        reg2_im1 <= (others => '0');
        reg2_im2 <= (others => '0');       
      elsif i_ena = '1' then
        -- sample inputs into register
        input_reg_a.im <= to_sfixed(i_a(31 downto 16), input_reg_a.im);
        input_reg_a.re <= to_sfixed(i_a(15 downto 0), input_reg_a.re);

        input_reg_b.im <= to_sfixed(i_b(31 downto 16), input_reg_b.im);
        input_reg_b.re <= to_sfixed(i_b(15 downto 0), input_reg_b.re);
        

        -- calculate products
        reg_re1 <= input_reg_a.re * input_reg_b.re;
        reg_re2 <= input_reg_a.im * input_reg_b.im;
        reg_im1 <= input_reg_a.re * input_reg_b.im;
        reg_im2 <= input_reg_a.im * input_reg_b.re;

        -- pipeline
        reg2_re1 <= resize(reg_re1, reg2_re1);
        reg2_re2 <= resize(reg_re2, reg2_re2);
        reg2_im1 <= resize(reg_im1, reg2_im1);
        reg2_im2 <= resize(reg_im2, reg2_im2);
        
        -- add the results and write output registers
        --o_z_real := resize(reg_re1, o_z_real'length) - resize(reg_re2, o_z_real'length);
        o_z_real := resize(reg2_re1 - reg2_re2, o_z_real);
        
        --o_z_imag := resize(reg_im1, o_z_imag'length) + resize(reg_im2, o_z_imag'length);
        o_z_imag := resize(reg2_im1 + reg2_im2, o_z_real);
        --o_z_imag := reg_im1 + reg_im2;

        o_z <= to_sulv(o_z_real) & to_sulv(o_z_imag);
      end if;
    end if;
  end process;

end architecture rtl;
