-- RFNoC interference (c) by David Haberleitner
--
-- RFNoC interference is licensed under a
-- Creative Commons Attribution-ShareAlike 4.0 International License.
--
-- You should have received a copy of the license along with this
-- work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.


library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity ram_2p is
  generic( g_width : integer := 32;
           g_addr_width : integer := 10;
           g_length : integer := 1024);
  
  port (o : out STD_ULOGIC_VECTOR(g_width - 1 downto 0);
        re, we, clk : in STD_ULOGIC;
        d : in STD_ULOGIC_VECTOR(g_width - 1 downto 0);
        raddr, waddr : in STD_ULOGIC_VECTOR(g_addr_width - 1 downto 0));

end ram_2p;

architecture rtl of ram_2p is
  type mem_type is array (g_length - 1 downto 0) of
    STD_ULOGIC_VECTOR (g_width - 1 downto 0);

  signal mem : mem_type := (others => (others => '0'));
  --signal rd_data : std_ulogic_vector(g_width - 1 downto 0);  -- read data delay

begin

  process(clk)
  begin
    if (rising_edge(clk)) then
      if re = '1' then
        o <= mem(to_integer(unsigned(raddr)));
      end if;
      
      if (we = '1') then
        mem(to_integer(unsigned(waddr))) <= d;
      end if;
      
    end if;
  end process;

end rtl;
