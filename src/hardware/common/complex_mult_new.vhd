-- RFNoC interference (c) by David Haberleitner
--
-- RFNoC interference is licensed under a
-- Creative Commons Attribution-ShareAlike 4.0 International License.
--
-- You should have received a copy of the license along with this
-- work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_pkg.all;
-- use work.fixed_pkg.all;


entity complex_mult_new is
  
  port (
    i_clk : in std_ulogic;
    i_rst : in std_ulogic;
    i_ena : in std_ulogic;
    i_a : in std_ulogic_vector(31 downto 0);
    i_b : in std_ulogic_vector(31 downto 0);
    o_z : out std_ulogic_vector(31 downto 0)
    );
  
end entity complex_mult_new;

architecture rtl of complex_mult_new is

  signal re1 : std_ulogic_vector(15 downto 0);
  signal re2 : std_ulogic_vector(15 downto 0);

  signal im1 : std_ulogic_vector(15 downto 0);
  signal im2 : std_ulogic_vector(15 downto 0);
  
begin  -- architecture rtl

  
  -- purpose: multiplication of a axi streaming data channel with an always valid coefficient
  -- type   : sequential
  -- inputs : i_clk, i_rst
  -- outputs: o_tdata
  process (i_clk, i_rst) is
    variable o_z_real : std_ulogic_vector(15 downto 0);
    variable o_z_imag : std_ulogic_vector(15 downto 0);


  begin  -- process
    if i_clk'event and i_clk = '1' then  -- rising clock edge
      if i_rst = '1' then                 -- asynchronous reset (active low)
        o_z <= (others => '0');
      elsif i_ena = '1' then

        -- add the results and write output registers
        o_z_real := std_ulogic_vector(resize(signed(re1) - signed(re2), o_z_real'length));        
        o_z_imag := std_ulogic_vector(resize(signed(im1) + signed(im2), o_z_real'length));

        o_z <= (o_z_real & o_z_imag);
      end if;
    end if;
  end process;

  -- a_re * b_re
  mult_16x16_1: entity work.mult_16x16
    port map (
      clk => i_clk,
      rst => i_rst,
      ena => i_ena,
      Ain => i_a(15 downto 0),
      Bin => i_b(15 downto 0),
      P   => re1);

  -- a_im * b_im
  mult_16x16_2: entity work.mult_16x16
    port map (
      clk => i_clk,
      rst => i_rst,
      ena => i_ena,
      Ain => i_a(31 downto 16),
      Bin => i_b(31 downto 16),
      P   => re2);

  -- a_re * b_im
  mult_16x16_3: entity work.mult_16x16
    port map (
      clk => i_clk,
      rst => i_rst,
      ena => i_ena,
      Ain => i_a(15 downto 0),
      Bin => i_b(31 downto 16),
      P   => im1);

  -- a_im * b_re
  mult_16x16_4: entity work.mult_16x16
    port map (
      clk => i_clk,
      rst => i_rst,
      ena => i_ena,
      Ain => i_a(31 downto 16),
      Bin => i_b(15 downto 0),
      P   => im2);
  
end architecture rtl;
