-- RFNoC interference (c) by David Haberleitner
--
-- RFNoC interference is licensed under a
-- Creative Commons Attribution-ShareAlike 4.0 International License.
--
-- You should have received a copy of the license along with this
-- work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity lfsr8 is
  
  port (
    i_clk    : in  std_ulogic;
    i_rst    : in  std_ulogic;
    o_rand : out std_ulogic_vector(7 downto 0));

end entity lfsr8;


architecture rtl of lfsr8 is
  signal fb : std_ulogic;
  signal reg : std_ulogic_vector(7 downto 0);
begin  -- architecture rtl

  -- purpose: shift reg
  -- type   : sequential
  -- inputs : i_clk, i_rst
  -- outputs: o_rand
  reg_proc: process (i_clk, i_rst) is
  begin  -- process reg
    if i_rst = '1' then                 -- asynchronous reset (active low)
      reg <= (0 => '1', others => '0');
    elsif i_clk'event and i_clk = '1' then  -- rising clock edge
      reg <= (reg(0) xor reg(2) xor reg(3) xor reg(4)) & reg(7 downto 1);
    end if;
  end process reg_proc;

  o_rand <= reg;
  
end architecture rtl;
