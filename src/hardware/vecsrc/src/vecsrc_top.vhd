-- RFNoC interference (c) by David Haberleitner
--
-- RFNoC interference is licensed under a
-- Creative Commons Attribution-ShareAlike 4.0 International License.
--
-- You should have received a copy of the license along with this
-- work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- use work.vecsrc_pkg.all;

entity vecsrc_top is

  port (
    clk_i : in std_ulogic;
    rst_i : in std_ulogic;

    -- Input stream
    -- tdata_i  : in  std_ulogic_vector(31 downto 0);
    -- tready_i : out std_ulogic;
    -- tvalid_i : in  std_ulogic;
    -- tlast_i  : in  std_ulogic;

    -- Output stream
    o_tdata  : out std_ulogic_vector(31 downto 0);
    o_tready : in  std_ulogic;
    o_tvalid : out std_ulogic;
    o_tlast  : out std_ulogic;

    -- Config stream
    tdata_conf  : in std_ulogic_vector(31 downto 0);
    tready_conf : out  std_ulogic;
    tvalid_conf : in std_ulogic;
    tlast_conf  : in std_ulogic;


    -- Register address
    sb_addr_i   : in std_ulogic_vector(7 downto 0);
    -- Set strobe
    sb_strobe_i : in std_ulogic;
    -- Set data
    sb_data_i   : in std_ulogic_vector(31 downto 0);
    -- Readback data
    sb_rbdata_o : out std_ulogic_vector(31 downto 0));

end entity vecsrc_top;


architecture rtl of vecsrc_top is

begin
  
  vecsrc_impl_1: entity work.vecsrc_impl
    port map (
      i_clk       => clk_i,
      i_rst       => rst_i,
      o_tdata     => o_tdata,
      o_tready    => o_tready,
      o_tvalid    => o_tvalid,
      o_tlast     => o_tlast,
      conf_coeff_tdata  => tdata_conf,
      conf_coeff_tready => tready_conf,
      conf_coeff_tvalid => tvalid_conf,
      conf_coeff_tlast  => tlast_conf);

end architecture rtl;
