-- RFNoC interference (c) by David Haberleitner
--
-- RFNoC interference is licensed under a
-- Creative Commons Attribution-ShareAlike 4.0 International License.
--
-- You should have received a copy of the license along with this
-- work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity vecsrc_impl is
  port (
    i_clk : in std_ulogic;
    i_rst : in std_ulogic;


    -- output interface
    o_tdata  : out std_ulogic_vector(31 downto 0);
    o_tready : in  std_ulogic;
    o_tvalid : out std_ulogic;
    o_tlast  : out std_ulogic;

    -- config interface coefficients
    conf_coeff_tdata  : in  std_ulogic_vector(31 downto 0);
    conf_coeff_tready : out std_ulogic;
    conf_coeff_tvalid : in  std_ulogic;
    conf_coeff_tlast  : in  std_ulogic;

    -- config interface magnitude LUT
    conf_mag_tdata  : in  std_ulogic_vector(31 downto 0);
    conf_mag_tready : out std_ulogic;
    conf_mag_tvalid : in  std_ulogic;
    conf_mag_tlast  : in  std_ulogic;

    -- config interface phase LUT
    conf_phase_tdata  : in  std_ulogic_vector(31 downto 0);
    conf_phase_tready : out std_ulogic;
    conf_phase_tvalid : in  std_ulogic;
    conf_phase_tlast  : in  std_ulogic;

    o_lines_left : out std_ulogic_vector(3 downto 0)

    );
end entity vecsrc_impl;

architecture rtl of vecsrc_impl is

  type t_state is (start, wr_coeffs, wr_duration, wr_phase_sel);
  type t_duration_arr is array (15 downto 0) of unsigned(31 downto 0);

  type reg_set_t is record
    state                   : t_state;
    sample_idx_out          : unsigned(9 downto 0);
    sample_idx_in           : unsigned(7 downto 0);
    --empty : std_ulogic;
    --full : std_ulogic;
    phase_sel_valid         : std_ulogic_vector(15 downto 0);
    lines_left              : unsigned(3 downto 0);
    field_idx_out           : unsigned(3 downto 0);
    field_idx_in            : unsigned(3 downto 0);
    duration_mem            : t_duration_arr;
    write_duration          : std_ulogic;
    valid_pipe              : std_ulogic_vector(6 downto 0);
    last_pipe               : std_ulogic_vector(6 downto 0);
    select_pipe             : unsigned(1 downto 0);
    phase_sel_valid_delayed : std_ulogic;
    phase_sel : std_ulogic_vector(7 downto 0);
    mag_lin       : std_ulogic_vector(31 downto 0);
  end record;

  constant reset_val_c : reg_set_t := (
    state                   => start,
    sample_idx_out          => (others => '0'),
    sample_idx_in           => (others => '0'),
    --empty => '1',
    --full => '0',
    phase_sel_valid         => (others => '0'),
    lines_left              => to_unsigned(15, 4),
    field_idx_out           => (others => '0'),
    field_idx_in            => (others => '0'),
    duration_mem            => (others => (others => '0')),
    write_duration          => '0',
    valid_pipe              => (others => '0'),
    last_pipe               => (others => '0'),
    select_pipe             => (others => '0'),
    phase_sel_valid_delayed => '0',
    phase_sel => (others => '0'),
    mag_lin => (others => '0')
    );

  signal reg, nxt_reg : reg_set_t;

  signal we        : std_ulogic;
  signal ram_idata : std_ulogic_vector(7 downto 0);

  signal rd_addr   : std_ulogic_vector(11 downto 0);
  signal wr_addr   : std_ulogic_vector(11 downto 0);
  signal ram_odata : std_ulogic_vector(31 downto 0);

  signal phase_sel_we    : std_ulogic;
  signal phase_sel_odata : std_ulogic_vector(31 downto 0);

  signal rand      : std_ulogic_vector(7 downto 0);

  signal mag_lut_raddr : std_ulogic_vector(7 downto 0);
  signal mag_lut_waddr : std_ulogic_vector(7 downto 0);
  signal mag_lin       : std_ulogic_vector(31 downto 0);

  signal phase_lut_raddr : std_ulogic_vector(7 downto 0);
  signal phase_lut_waddr : std_ulogic_vector(7 downto 0);
  signal phase_out       : std_ulogic_vector(31 downto 0);

begin  -- architecture rtl

  -- combine field selection and sample index into one address
  rd_addr <= (std_ulogic_vector(reg.field_idx_out) & std_ulogic_vector(reg.sample_idx_out(9 downto 2)));
  wr_addr <= (std_ulogic_vector(reg.field_idx_in) & std_ulogic_vector(reg.sample_idx_in));

  o_lines_left <= std_ulogic_vector(reg.lines_left);

  rams_2p_1 : entity work.ram_2p
    generic map (
      g_width      => 32,
      g_addr_width => 8+4,
      g_length     => 256*16)
    port map (
      clk   => i_clk,
      re => o_tready,
      we    => we,
      raddr => rd_addr,
      waddr => wr_addr,
      d     => conf_coeff_tdata,
      o     => ram_odata);

  mag_lut : entity work.ram_2p
    generic map (
      g_width      => 32,
      g_addr_width => 8,
      g_length     => 256)
    port map (
      clk   => i_clk,
      re => o_tready,
      we    => conf_mag_tvalid,
      raddr => mag_lut_raddr,
      waddr => mag_lut_waddr,
      d     => conf_mag_tdata,
      o     => mag_lin);

  lfsr8_1 : entity work.lfsr8
    port map (
      i_clk  => i_clk,
      i_rst  => i_rst,
      o_rand => rand);

  phase_lut : entity work.ram_2p
    generic map (
      g_width      => 32,
      g_addr_width => 8,
      g_length     => 256)
    port map (
      clk   => i_clk,
      re => o_tready,
      we    => conf_phase_tvalid,
      raddr => phase_lut_raddr,
      waddr => phase_lut_waddr,
      d     => conf_phase_tdata,
      o     => phase_out);

  phase_sel_mem : entity work.ram_2p
    generic map (
      g_width      => 32,
      g_addr_width => 8+4,
      g_length     => 256*16)
    port map (
      clk   => i_clk,
      re => o_tready,
      we    => phase_sel_we,
      raddr => rd_addr,  -- same read address as for coefficients
      waddr => wr_addr,  -- also the same (gets looped two times when writing
      -- phase_sel and coeffs)
      d     => conf_coeff_tdata,
      o     => phase_sel_odata);

  -- purpose: write magnitude LUT
  -- type   : sequential
  -- inputs : i_clk, i_rst
  -- outputs: mag_lut_waddr
  write_mag_lut : process (i_clk, i_rst) is
  begin  -- process write_mag_lut
    if i_clk'event and i_clk = '1' then  -- rising clock edge
      if i_rst = '1' then                -- synchronous reset (active high)
        mag_lut_waddr <= (others => '0');
      else
        if conf_mag_tvalid = '1' then
          mag_lut_waddr <= std_ulogic_vector(unsigned(mag_lut_waddr) + 1);
        end if;
      end if;
      -- TODO: reset counter on tlast for better syn?

    end if;
  end process write_mag_lut;

  -- purpose: write phase LUT
  -- type   : sequential
  -- inputs : i_clk, i_rst
  -- outputs: phase_lut_waddr
  write_phase_lut : process (i_clk, i_rst) is
  begin  -- process write_phase_lut
    if i_clk'event and i_clk = '1' then  -- rising clock edge
      if i_rst = '1' then                -- synchronous reset (active high)
        phase_lut_waddr <= (others => '0');
      else
        if conf_phase_tvalid = '1' then
          phase_lut_waddr <= std_ulogic_vector(unsigned(phase_lut_waddr) + 1);
        end if;
      end if;
    end if;
  end process write_phase_lut;


  -- always ready for data
  conf_mag_tready   <= '1';
  conf_phase_tready <= '1';
  --o_tdata <= mag_lin;

  -- purpose: register process
  -- type   : sequential
  -- inputs : i_clk, i_rst
  -- outputs: 
  reg_proc : process (i_clk, i_rst) is
  begin  -- process reg_proc
    if i_clk'event and i_clk = '1' then  -- rising clock edge
      if i_rst = '1' then                -- synchronous reset (active high)
        reg <= reset_val_c;
      else
        reg <= nxt_reg;
      end if;
    end if;
  end process reg_proc;

  -- purpose: state machine transistions
  -- type   : combinational
  -- inputs : reg
  -- outputs: 
  comb : process (reg, o_tready, conf_coeff_tdata, conf_coeff_tvalid, conf_coeff_tlast, phase_sel_odata, mag_lin) is
  begin  -- process comb

    -- default assignments
    nxt_reg      <= reg;
    o_tlast      <= '0';
    we           <= '0';
    phase_sel_we <= '0';

    o_tvalid     <= '0';

    -- consume samples from the config bus whenever there is data
    -- TODO: set tready only when fifo is not full
    conf_coeff_tready <= '1';

    -- delay phase sel valid for one cycle
    nxt_reg.phase_sel_valid_delayed <= reg.phase_sel_valid(to_integer(reg.field_idx_out));
    nxt_reg.mag_lin <= mag_lin;

    -- halt the pipeline if the downstream block is not ready
    if o_tready = '1' then
      o_tvalid                       <= reg.valid_pipe(6);
      nxt_reg.valid_pipe(6 downto 1) <= reg.valid_pipe(5 downto 0);
      nxt_reg.valid_pipe(0)          <= '0';

      o_tlast                       <= reg.last_pipe(6);
      nxt_reg.last_pipe(6 downto 1) <= reg.last_pipe(5 downto 0);
      nxt_reg.last_pipe(0)          <= '0';
    end if;

    nxt_reg.select_pipe <= reg.sample_idx_out(1 downto 0);
    --o_tdata(31 downto 8) <= (others => '0');
    case to_integer(reg.select_pipe) is
      when 0 =>
        mag_lut_raddr <= ram_odata(7 downto 0);
        nxt_reg.phase_sel     <= phase_sel_odata(7 downto 0);
      when 1 =>
        mag_lut_raddr <= ram_odata(15 downto 8);
        nxt_reg.phase_sel     <= phase_sel_odata(15 downto 8);
      when 2 =>
        mag_lut_raddr <= ram_odata(23 downto 16);
        nxt_reg.phase_sel     <= phase_sel_odata(23 downto 16);
      when 3 =>
        mag_lut_raddr <= ram_odata(31 downto 24);
        nxt_reg.phase_sel     <= phase_sel_odata(31 downto 24);
      when others => null;
    end case;


    -- select random phase when:
    --  phase_select is zero
    --  or current line has no valid phase selection 
    if reg.phase_sel_valid_delayed = '1' then
      case to_integer(unsigned(reg.phase_sel)) is
        when 0 =>
          phase_lut_raddr <= rand;
        when others =>
          phase_lut_raddr <= reg.phase_sel;
      end case;
    else
      phase_lut_raddr <= rand;
    end if;


    case reg.state is
      when start =>
        -- select the next state by reading the type "sample"
        if (conf_coeff_tvalid = '1') then
          case to_integer(unsigned(conf_coeff_tdata)) is
            when 0 =>
              nxt_reg.state <= wr_phase_sel;
            when others =>
              nxt_reg.state <= wr_coeffs;
          end case;
        end if;

      when wr_phase_sel =>
        if (conf_coeff_tvalid = '1') and (reg.lines_left > 0) then
          phase_sel_we          <= '1';
          nxt_reg.sample_idx_in <= reg.sample_idx_in + 1;

          -- phase sel finished, write coeffs field next
          if reg.sample_idx_in = 256-1 then
            nxt_reg.state                                         <= start;
            nxt_reg.phase_sel_valid(to_integer(reg.field_idx_in)) <= '1';
          end if;
        end if;
      when wr_coeffs =>
        if (conf_coeff_tvalid = '1') and (reg.lines_left > 0) then
          we                    <= '1';
          nxt_reg.sample_idx_in <= reg.sample_idx_in + 1;

          -- coeffs finished, write duration field next
          if reg.sample_idx_in = 256-1 then
            nxt_reg.state <= wr_duration;
          end if;
        end if;

      when wr_duration =>
        nxt_reg.duration_mem(to_integer(reg.field_idx_in)) <= unsigned(conf_coeff_tdata);

        -- write access finished
        nxt_reg.field_idx_in <= reg.field_idx_in + 1;
        --nxt_reg.empty <= '0';
        nxt_reg.lines_left   <= reg.lines_left - 1;

        nxt_reg.state <= start;

      when others => null;
    end case;

    -- provide data from the internal ram on the output when requested
    if o_tready = '1' and (reg.lines_left < 15) then
      nxt_reg.valid_pipe(0)  <= '1';
      nxt_reg.sample_idx_out <= reg.sample_idx_out + 1;

      -- is this the last sample in the vector?
      if to_integer(reg.sample_idx_out) = 1024-1 then
        --o_tlast            <= '1';
        nxt_reg.last_pipe(0)                                <= '1';
        nxt_reg.duration_mem(to_integer(reg.field_idx_out)) <= reg.duration_mem(to_integer(reg.field_idx_out)) - 1;

        -- this was the last packet for this coeff set
        if to_integer(reg.duration_mem(to_integer(reg.field_idx_out))) = 1 then
          nxt_reg.field_idx_out <= reg.field_idx_out + 1;
          nxt_reg.lines_left    <= reg.lines_left + 1;

          -- reset phase configuration for this line
          nxt_reg.phase_sel_valid(to_integer(reg.field_idx_out)) <= '0';
        end if;
      end if;
    end if;


  end process comb;


  complex_mult_1 : entity work.complex_mult_new
    port map (
      i_clk => i_clk,
      i_rst => i_rst,
      i_ena => o_tready,
      i_a   => reg.mag_lin,
      i_b   => phase_out,
      o_z   => o_tdata);

end architecture rtl;
