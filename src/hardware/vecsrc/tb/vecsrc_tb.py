import cocotb
from cocotb.binary import BinaryValue
from cocotb.triggers import Timer, RisingEdge, ReadOnly
from cocotb.scoreboard import Scoreboard
import sys
import pathlib
import numpy as np
import logging

# add path of busfunctional model
sys.path.append(str(pathlib.Path(__file__).parent.absolute()) + "/../../../cocotb")
from bfm import SettingsBusMaster,SettingsBusMonitor
from bfm import AXIStreamMaster, AXIStreamMonitor
from bfm import AXIConfMaster, AXIConfMonitor

class VecsrcModel(object):
    ADDRESS_GAIN = 0
    ADDRESS_COUNTER = 1

    def __init__(self):
        self.expected_output = []
        # self.gain = 0
        

        # self.registers = {self.ADDRESS_GAIN:0,
        #                   self.ADDRESS_COUNTER:0}


    def settings_bus_operation(self,transaction):
        pass


    def calculate(self,transaction:list):
        """
        Simple vecsrc block model (binary representation)
        """

        duration = transaction.pop().signed_integer

        print("Duration: {:d}".format(duration))
        print("Values left in transaction: {}".format(len(transaction)))

        for i in range(0, len(transaction)):
            print("Conf: {}: {}".format(i, transaction[i].signed_integer))

        for i in range(0,duration):
            self.expected_output.append(transaction)



class VecsrcTB(object):
    def __init__(self,dut: cocotb.handle.HierarchyObject):
        self.dut = dut
        self.dut._log.debug("Building/Connecting Testbench")

        self.model = VecsrcModel()
        
        # must connect recovermonitor before stream_out monitor, else error in scoreboard because model would not get called before 
        # hardware has done the calculation
        #self.recoverStream = AXIConfMonitor(dut,None,dut.i_clk,32,32,isInput=True,callback=self.model.calculate)
        self.recoverStream = AXIStreamMonitor(dut,None,dut.i_clk,32,32,isInput=True,callback=self.model.calculate,signal_prefix="conf_coeff")
        self.stream_config = AXIStreamMaster(dut,None,dut.i_clk,32,32,signal_prefix="conf_coeff")

        self.recover_mag_config = AXIStreamMonitor(dut,None,dut.i_clk,32,32,isInput=True,signal_prefix="conf_mag")
        self.stream_mag_config = AXIStreamMaster(dut,None,dut.i_clk,32,32,signal_prefix="conf_mag")

        self.recover_phase_config = AXIStreamMonitor(dut,None,dut.i_clk,32,32,isInput=True,signal_prefix="conf_phase")
        self.stream_phase_config = AXIStreamMaster(dut,None,dut.i_clk,32,32,signal_prefix="conf_phase")


        self.stream_out = AXIStreamMonitor(dut,None,dut.i_clk,32,32,callback=self.printRes,signal_prefix="o")
        #self.settingsMonitor   = SettingsBusMonitor(dut,"sb",dut.i_clk,32,8,callback=self.model.settings_bus_operation)

        self.scoreboard = Scoreboard(dut)
        #self.scoreboard.add_interface(self.stream_out,self.model.expected_output)


    def printRes(self, transaction:list):
        print("Got transaction with {} values".format(len(transaction)))
        i=0
        for item in transaction:
            #pass
            #if item.signed_integer != self.model.expected_output[0]:
            #    print("Expected Value does not match")
            # print("Received {}: {}".format(i, item.signed_integer))
            x = item[16:31].signed_integer / (2**15)
            y = item[0:15].signed_integer / (2**15)
            z = x + y*1j
            print("Received {}: {}, {}, abs: {}".format(i,x,y,abs(z)))
            i = i+1

    @cocotb.coroutine
    async def reset(self):
        
        self.dut.i_rst <= 0
        await RisingEdge(self.dut.i_clk)
        await self.stream_config.reset()
        #await self.settings.reset()

        self.dut.i_rst <= 1
        await Timer(100,units="ns")
        await RisingEdge(self.dut.i_clk)
        self.dut.i_rst <= 0
        await RisingEdge(self.dut.i_clk)
