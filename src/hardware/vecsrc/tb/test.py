
import numpy as np
import matplotlib.pyplot as plt
from math import e, pi
import random

import cocotb
from cocotb.binary import BinaryValue
from cocotb.triggers import Timer, RisingEdge, ReadOnly
from cocotb.clock import Clock
from vecsrc_tb import VecsrcTB


@cocotb.test()
def main_test(dut):
    dut._log.info("######### Start of test sequence #########")
    num_samples_in = int(1024/4)
    num_samples_out = 1024
    
    clk_1mhz   = Clock(dut.i_clk, 1.0, units='us')
    clk_gen = cocotb.fork(clk_1mhz.start())

    dut.o_tready <= 0
    
    # insantiate testbench and reset
    tb = VecsrcTB(dut)
    yield tb.reset()

    # unpacked_mag_sel = np.ones(num_samples_out, dtype=int)
    unpacked_mag_sel = np.linspace(1,255,num_samples_out, dtype=int)
    mag_sel = np.empty((num_samples_in),dtype=int)

    unpacked_phase_sel = np.ones(num_samples_out, dtype=int)
    unpacked_phase_sel[0] = 0
    unpacked_phase_sel[-1] = 0
    phase_sel = np.empty((num_samples_in),dtype=int)

    yield Timer(5,"us")

    # produce config coeff set
    for i in range(0, num_samples_in):
        mag_sel[i] = unpacked_mag_sel[i*4+0] | unpacked_mag_sel[i*4+1] << 8 | unpacked_mag_sel[i*4+2] << 16 | unpacked_mag_sel[i*4+3] << 24;
        phase_sel[i] = unpacked_phase_sel[i*4+0] | unpacked_phase_sel[i*4+1] << 8 | unpacked_phase_sel[i*4+2] << 16 | unpacked_phase_sel[i*4+3] << 24;

    # magconf = np.zeros(256,dtype=int)
    # magconf[1] = (2**15)-1
    magconf = np.linspace(0, (2**15)-1, 256, dtype=int)
    yield tb.stream_mag_config.write(magconf.tolist())
    
    phaseconf = np.empty(256,dtype=int)
    for i in range(0, 256):
        z = (2**15-1)*(e**(random.random()*2*pi*1j))
        print("{}, {}".format(z.real, z.imag))
        phaseconf[i] = (int(z.real) & 0xFFFF) | ((int(z.imag) & 0xFFFF)<<16)
    yield tb.stream_phase_config.write(phaseconf.tolist())


    #yield Timer(1,"us")


    # type: phase select
    phase_sel = np.insert(phase_sel, 0, 0)
    yield tb.stream_config.write(phase_sel.tolist())


    # type: coeffs
    mag_sel = np.insert(mag_sel, 0, 1)
    # duration
    mag_sel = np.append(mag_sel, 4)
    yield tb.stream_config.write(mag_sel.tolist())


    dut.o_tready <= 1

    yield Timer(1.5,"ms")
    dut.o_tready <= 0
    yield Timer(100,"us")
    dut.o_tready <= 1
    # produce config coeff set
    #mag_sel = np.arange(1025,dtype=int)
    #mag_sel = np.append(mag_sel, 1)

    # yield Timer(4,"us")
    # #yield tb.stream_config.write(mag_sel.tolist())
    # # write some values

    # mag_sel[-1] = 1
    # yield tb.stream_config.write(mag_sel.tolist())

    # yield Timer(0.1,"us")
    
    # mag_sel[-1] = 2
    # yield tb.stream_config.write(mag_sel.tolist())


    yield Timer(5,"ms")
    dut.o_tready <= 0

    # plt.figure()
    # plt.plot(out_signal)
    # plt.show()

    dut._log.info("######### End of test sequence #########")
