import numpy as np


VECLEN = 1024


class vecsrc:
    def __init__(self, quant, bits, clip):
        self.mag_table = np.zeros(VECLEN)
        self.phase_table = np.zeros(VECLEN)
        self.quant = quant
        self.bits = bits
        self.maxval = 2**(bits-1)
        self.clip = clip

    def set_mag_table(self, mag_table):
        self.mag_table = mag_table

    def set_phase_table(self, phase_table):
        self.phase_table = phase_table

    def work(self, mag_sel, repeats, phase_sel=None):

        # map input selections to complex magnitude value
        mag = self.mag_table[mag_sel]


        out = np.empty((VECLEN, repeats), dtype=complex)
        
        # loop over repeats to assign a new phase at every iteration
        for i in range(0, repeats):
            # multiply every element with a random phase or the phase selected by phase_sel
            if phase_sel is not None:
                # TODO vectorize
                for sample_idx in range(0, len(mag_sel)):
                    if phase_sel[sample_idx] == 0:
                        out[sample_idx, i] = mag[sample_idx] * self.phase_table[np.random.randint(1, 256)]
                    else:
                        out[sample_idx, i] = mag[sample_idx] * self.phase_table[phase_sel[i]]
            else:
                out[:, i] = mag * self.phase_table[np.random.randint(1, 256, size=VECLEN)]

        if self.quant:
            out = np.around(out * self.maxval) / self.maxval

        if self.clip:
            # clip in-place
            np.clip(out, -1, 1, out) 
        return out
