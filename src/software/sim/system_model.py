import sys
import numpy as np
import matplotlib.pyplot as plt
import argparse

from vecsrc import vecsrc
from fadewin import fadewin

fs = 200e6
spp = 1024
bits = 16

maxval = 2**(bits-1)

quant = True
clip = True

class interference_gen:
    def __init__(self, mag_file, phase_file, win_file, scen_file):
        self.vecsrc = vecsrc(quant, bits, clip)
        self.fadewin = fadewin(quant, bits, clip)

        # read complex csv files
        mag_table = np.genfromtxt(mag_file, delimiter=',') / maxval
        mag_table = mag_table[:,0] + 1j * mag_table[:,1]
        phase_table = np.genfromtxt(phase_file, delimiter=',') / maxval
        phase_table = phase_table[:,0] + 1j * phase_table[:,1]
        window = np.genfromtxt(win_file, delimiter=',') / maxval

        self.vecsrc.set_mag_table(mag_table)
        self.vecsrc.set_phase_table(phase_table)

        self.fadewin.set_window(window)

        # read scenario file
        scenario = np.genfromtxt(scen_file, delimiter=',', dtype=str)
        line_type = scenario[:,0]
        scenario = np.genfromtxt(scen_file, delimiter=',', dtype='<i8')
        scenario_data = scenario[:,1:]

        nr_vectors = np.sum(scenario_data[:,-1])
        vecsrc_out = np.empty((1024,nr_vectors),dtype=complex)

        out_vec_idx = 0
        phase_sel = None
        for i in range(0, len(line_type)):
            if line_type[i] == 'phase':
                # extract phase
                phase_sel = scenario_data[i]
                
            if line_type[i] == 'mag':
                # extract coefficients from columns
                mag_sel = scenario_data[i,0:-1]

                reps = scenario_data[i,-1]
                # produce samples from current line
                out = self.vecsrc.work(mag_sel, reps, phase_sel)
                # reset phase selection
                phase_sel = None

                # build the complete output signal
                vecsrc_out[:,out_vec_idx:out_vec_idx+reps] = out
                out_vec_idx = out_vec_idx + reps

        # plt.figure()
        # plt.plot(vecsrc_out)
        # plt.show()

        ifft_out = np.empty_like(vecsrc_out)

        for i in range(0, nr_vectors):
            ifft_out[:, i] = np.fft.ifft(np.fft.ifftshift(vecsrc_out[:, i]))

        if quant:
            ifft_out = np.around(ifft_out * maxval) / maxval
        if clip:
            # clip in-place
            np.clip(ifft_out, -1, 1, ifft_out) 

        # plt.figure()
        # plt.plot(np.abs(ifft_out))
        # plt.show()


        fadewin_out = self.fadewin.work(ifft_out)
        
        plt.figure()
        plt.specgram(fadewin_out.T.flatten(), NFFT=2048, Fs=fs, scale='dB', mode='magnitude')
        plt.show()

# Construct the argument parser
ap = argparse.ArgumentParser()


ap.add_argument("-m", "--mag_file", required=True,
   help="magnitude table coefficients filename")
ap.add_argument("-p", "--phase_file", required=True,
   help="phase table coefficients filename")
ap.add_argument("-w", "--win_file", required=True,
   help="window coefficients filename")
ap.add_argument("-s", "--scenario_file", required=True,
   help="scenario filename")
args = vars(ap.parse_args())

dut = interference_gen(args["mag_file"], args["phase_file"], args["win_file"], args["scenario_file"])

