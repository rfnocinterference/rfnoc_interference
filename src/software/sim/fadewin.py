import numpy as np


VECLEN = 1024


class fadewin:
    def __init__(self, quant, bits, clip):
        self.window = np.zeros(VECLEN)
        self.oldvec = np.zeros(VECLEN, dtype=complex)
        self.quant = quant
        self.bits = bits
        self.maxval = 2**(bits-1)
        self.clip = clip

    def set_window(self, window):
        self.window = window


    def work(self, signal):

        out = np.empty_like(signal, dtype=complex)

        # iterate over vectors
        for i in range(0, signal.shape[1]):
            out[:,i] = signal[:,i] * self.window + self.oldvec * (1-self.window)
            self.oldvec = signal[:,i]

        if self.quant:
            out = np.around(out*self.maxval) / self.maxval
        if self.clip:
            # clip in-place
            np.clip(out, -1, 1, out) 
        return out
