# RFNoC interference (c) by David Haberleitner
# 
# RFNoC interference is licensed under a
# Creative Commons Attribution-ShareAlike 4.0 International License.
#
# You should have received a copy of the license along with this
# work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>. 

import argparse
import numpy as np
from math import e, pi
from PIL import Image
import matplotlib.pyplot as plt


def stretch(x, min_i, max_i, min_o, max_o):
    x = (x-min_i) * (((max_o-min_o)/(max_i-min_i))+min_o)
    return x

# Construct the argument parser
ap = argparse.ArgumentParser()

# Add the arguments to the parser
ap.add_argument("-i", "--infile", required=True,
   help="input filename")
ap.add_argument("-o", "--outfile", required=True,
   help="output filename")
ap.add_argument("-r", "--rate", required=True,
   help="sample rate at which the frontend is running")
ap.add_argument("-d", "--duration", required=True,
   help="duration in which the whole image will be displayed")

args = vars(ap.parse_args())

rate = float(args['rate'])
duration = float(args['duration'])
nr_coeffs = 1024

# clip maximum value to avoid non linearities on bright lines
# clip = 110
min_o = 0
max_o = 120

pil_im = Image.open(args['infile']).convert('L').rotate(180)


# scale to number of coeffs for width
width = pil_im.width
new_width = nr_coeffs
new_height = int(pil_im.height * (new_width/width))

im = np.array(pil_im.resize((new_width, new_height)))

# calculate duration for one line
line_repeats = int((duration / new_height) / (nr_coeffs/rate))

if (duration / new_height) < 20e-6:
    print("Warning: time of single line is under 20us, 1GbE probably can't keep up when replaying")

print(line_repeats)

repeats_arr = np.ones((new_height,1))*line_repeats

im = stretch(im, np.min(im), np.max(im), min_o, max_o)
#im = np.clip(im,0,clip)

out = np.append(im, repeats_arr, axis=1)

print(out.shape)

# out_str = np.array_str(out)
out_str = np.char.mod('%d', out)

# these lines are magnitude configurations
out_str = np.insert(out_str, 0, "mag", axis=1)

np.savetxt(args['outfile'],out_str, delimiter=',', fmt='%s')

plt.figure()
plt.imshow(im)
plt.show()


