/* -*- c++ -*- */

#define INTERFERENCE_API
#define ETTUS_API

%include "gnuradio.i"/*			*/// the common stuff

//load generated python docstrings
%include "interference_swig_doc.i"
//Header from gr-ettus
%include "ettus/device3.h"
%include "ettus/rfnoc_block.h"
%include "ettus/rfnoc_block_impl.h"

%{
#include "ettus/device3.h"
#include "ettus/rfnoc_block_impl.h"
#include "interference/fadewin.h"
#include "interference/vecsrc.h"
#include "interference/peakdet.h"
%}

%include "interference/fadewin.h"
GR_SWIG_BLOCK_MAGIC2(interference, fadewin);
%include "interference/vecsrc.h"
GR_SWIG_BLOCK_MAGIC2(interference, vecsrc);
%include "interference/peakdet.h"
GR_SWIG_BLOCK_MAGIC2(interference, peakdet);
