
//
/* 
 * Copyright 2020 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

//
module noc_block_vecsrc #(
  parameter NOC_ID = 64'h25F8D4086E1D8C87,
  parameter STR_SINK_FIFOSIZE = 11)
(
  input bus_clk, input bus_rst,
  input ce_clk, input ce_rst,
  input  [63:0] i_tdata, input  i_tlast, input  i_tvalid, output i_tready,
  output [63:0] o_tdata, output o_tlast, output o_tvalid, input  o_tready,
  output [63:0] debug
);

  ////////////////////////////////////////////////////////////
  //
  // RFNoC Shell
  //
  ////////////////////////////////////////////////////////////
  wire [31:0] set_data;
  wire [7:0]  set_addr;
  wire        set_stb;
  reg  [63:0] rb_data;
  wire [7:0]  rb_addr;

  wire [63:0] cmdout_tdata, ackin_tdata;
  wire        cmdout_tlast, cmdout_tvalid, cmdout_tready, ackin_tlast, ackin_tvalid, ackin_tready;

  wire [63:0] str_sink_tdata, str_src_tdata;
  wire        str_sink_tlast, str_sink_tvalid, str_sink_tready, str_src_tlast, str_src_tvalid, str_src_tready;

  wire [15:0] src_sid;
  wire [15:0] next_dst_sid, resp_out_dst_sid;
  wire [15:0] resp_in_dst_sid;

  wire        clear_tx_seqnum;

  noc_shell #(
    .NOC_ID(NOC_ID),
    .STR_SINK_FIFOSIZE(STR_SINK_FIFOSIZE))
  noc_shell (
    .bus_clk(bus_clk), .bus_rst(bus_rst),
    .i_tdata(i_tdata), .i_tlast(i_tlast), .i_tvalid(i_tvalid), .i_tready(i_tready),
    .o_tdata(o_tdata), .o_tlast(o_tlast), .o_tvalid(o_tvalid), .o_tready(o_tready),
    // Computer Engine Clock Domain
    .clk(ce_clk), .reset(ce_rst),
    // Control Sink
    .set_data(set_data), .set_addr(set_addr), .set_stb(set_stb), .set_time(), .set_has_time(),
    .rb_stb(1'b1), .rb_data(rb_data), .rb_addr(rb_addr),
    // Control Source
    .cmdout_tdata(cmdout_tdata), .cmdout_tlast(cmdout_tlast), .cmdout_tvalid(cmdout_tvalid), .cmdout_tready(cmdout_tready),
    .ackin_tdata(ackin_tdata), .ackin_tlast(ackin_tlast), .ackin_tvalid(ackin_tvalid), .ackin_tready(ackin_tready),
    // Stream Sink
    .str_sink_tdata(str_sink_tdata), .str_sink_tlast(str_sink_tlast), .str_sink_tvalid(str_sink_tvalid), .str_sink_tready(str_sink_tready),
    // Stream Source
    .str_src_tdata(str_src_tdata), .str_src_tlast(str_src_tlast), .str_src_tvalid(str_src_tvalid), .str_src_tready(str_src_tready),
    // Stream IDs set by host
    .src_sid(src_sid),                   // SID of this block
    .next_dst_sid(next_dst_sid),         // Next destination SID
    .resp_in_dst_sid(resp_in_dst_sid),   // Response destination SID for input stream responses / errors
    .resp_out_dst_sid(resp_out_dst_sid), // Response destination SID for output stream responses / errors
    // Misc
    .vita_time('d0), .clear_tx_seqnum(clear_tx_seqnum),
    .debug(debug));

  ////////////////////////////////////////////////////////////
  //
  // AXI Wrapper
  // Convert RFNoC Shell interface into AXI stream interface
  //
  ////////////////////////////////////////////////////////////
   localparam NUM_AXI_CONFIG_BUS = 2;

   wire [31:0] s_axis_data_tdata;
   wire [127:0] s_axis_data_tuser;
   wire       s_axis_data_tlast;
   wire       s_axis_data_tvalid;
   wire       s_axis_data_tready;
   

   wire [31:0]  m_axis_data_tdata;
   wire [127:0] m_axis_data_tuser;
   wire         m_axis_data_tvalid;
   wire         m_axis_data_tready;
   wire         m_axis_data_tlast;

   wire [31:0] m_axis_config_mag_tdata;
   wire        m_axis_config_mag_tvalid;
   wire        m_axis_config_mag_tready;
   wire        m_axis_config_mag_tlast;

   wire [31:0] m_axis_config_phase_tdata;
   wire        m_axis_config_phase_tvalid;
   wire        m_axis_config_phase_tready;
   wire        m_axis_config_phase_tlast;

   
   // Null sink
   //assign str_sink_tready = 1'b1;


   localparam AXI_WRAPPER_BASE    = 128;
   localparam SR_AXI_CONFIG_BASE  = AXI_WRAPPER_BASE + 1;

  axi_wrapper #(
		.SIMPLE_MODE(0),
		.SR_AXI_CONFIG_BASE(SR_AXI_CONFIG_BASE),
		.NUM_AXI_CONFIG_BUS(NUM_AXI_CONFIG_BUS))
   axi_wrapper (
    .clk(ce_clk), .reset(ce_rst),
    .bus_clk(bus_clk), .bus_rst(bus_rst),
    .clear_tx_seqnum(clear_tx_seqnum),
    .next_dst(next_dst_sid),
    .set_stb(set_stb), .set_addr(set_addr), .set_data(set_data),
    .i_tdata(str_sink_tdata), .i_tlast(str_sink_tlast), .i_tvalid(str_sink_tvalid), .i_tready(str_sink_tready),
    .o_tdata(str_src_tdata), .o_tlast(str_src_tlast), .o_tvalid(str_src_tvalid), .o_tready(str_src_tready),
    .m_axis_data_tdata(m_axis_data_tdata),
    .m_axis_data_tlast(m_axis_data_tlast),
    .m_axis_data_tvalid(m_axis_data_tvalid),
    .m_axis_data_tready(m_axis_data_tready),
    .m_axis_data_tuser(m_axis_data_tuser),
    .s_axis_data_tdata(s_axis_data_tdata),
    .s_axis_data_tlast(s_axis_data_tlast),
    .s_axis_data_tvalid(s_axis_data_tvalid),
    .s_axis_data_tready(s_axis_data_tready),
    .s_axis_data_tuser(s_axis_data_tuser),
		
    .m_axis_config_tdata({m_axis_config_phase_tdata, m_axis_config_mag_tdata}),
    .m_axis_config_tlast({m_axis_config_phase_tlast, m_axis_config_mag_tlast}),
    .m_axis_config_tvalid({m_axis_config_phase_tvalid, m_axis_config_mag_tvalid}),
    .m_axis_config_tready({m_axis_config_phase_tready, m_axis_config_mag_tready}),
		
    .m_axis_pkt_len_tdata(),
    .m_axis_pkt_len_tvalid(),
    .m_axis_pkt_len_tready());

   cvita_hdr_encoder cvita_hdr_encoder (
					.pkt_type(2'd0), .eob(1'b0), .has_time(1'b0),
					.seqnum(12'd0), .payload_length(16'd0), 
					.dst_sid(next_dst_sid), .src_sid(src_sid),
					.vita_time(64'd0),
					.header(s_axis_data_tuser));

   ////////////////////////////////////////////////////////////
   //
   // User code
   //
   ////////////////////////////////////////////////////////////
   // NoC Shell registers 0 - 127,
   // User register address space starts at 128
   
   localparam SR_USER_REG_BASE = 128; // 129, 130, 131, 132 used by config bus

   
  // Control Source Unused
  assign cmdout_tdata  = 64'd0;
  assign cmdout_tlast  = 1'b0;
  assign cmdout_tvalid = 1'b0;
  assign ackin_tready  = 1'b1;

  // Settings registers
  //
  // - The settings register bus is a simple strobed interface.
  // - Transactions include both a write and a readback.
  // - The write occurs when set_stb is asserted.
  //   The settings register with the address matching set_addr will
  //   be loaded with the data on set_data.
  // - Readback occurs when rb_stb is asserted. The read back strobe
  //   must assert at least one clock cycle after set_stb asserts /
  //   rb_stb is ignored if asserted on the same clock cycle of set_stb.
  //   Example valid and invalid timing:
  //              __    __    __    __
  //   clk     __|  |__|  |__|  |__|  |__
  //               _____
  //   set_stb ___|     |________________
  //                     _____
  //   rb_stb  _________|     |__________     (Valid)
  //                           _____
  //   rb_stb  _______________|     |____     (Valid)
  //           __________________________
  //   rb_stb                                 (Valid if readback data is a constant)
  //               _____
  //   rb_stb  ___|     |________________     (Invalid / ignored, same cycle as set_stb)
  //
  localparam [7:0] SR_TEST_REG_0 = SR_USER_REG_BASE;

  wire [31:0] test_reg_0;
  setting_reg #(
    .my_addr(SR_TEST_REG_0), .awidth(8), .width(32))
  sr_test_reg_0 (
    .clk(ce_clk), .rst(ce_rst),
    .strobe(set_stb), .addr(set_addr), .in(set_data), .out(test_reg_0), .changed());

  wire [3:0] lines_left;
   

  // Readback registers
  // rb_stb set to 1'b1 on NoC Shell
  always @(posedge ce_clk) begin
    case(rb_addr)
      8'd0 : rb_data <= {32'd0, test_reg_0};
      8'd1 : rb_data <= {60'd0, lines_left};
      default : rb_data <= 64'h0BADC0DE0BADC0DE;
    endcase
  end


   vecsrc_impl block1(
		      .i_clk(ce_clk),
		      .i_rst(ce_rst),

		      .conf_coeff_tdata(m_axis_data_tdata),
		      .conf_coeff_tready(m_axis_data_tready),
		      .conf_coeff_tvalid(m_axis_data_tvalid),
		      .conf_coeff_tlast(m_axis_data_tlast),

		      .conf_mag_tdata(m_axis_config_mag_tdata),
		      .conf_mag_tready(m_axis_config_mag_tready),
		      .conf_mag_tvalid(m_axis_config_mag_tvalid),
		      .conf_mag_tlast(m_axis_config_mag_tlast),
		      
		      .conf_phase_tdata(m_axis_config_phase_tdata),
		      .conf_phase_tready(m_axis_config_phase_tready),
		      .conf_phase_tvalid(m_axis_config_phase_tvalid),
		      .conf_phase_tlast(m_axis_config_phase_tlast),
		      
		      .o_tdata(s_axis_data_tdata),
		      .o_tlast(s_axis_data_tlast),
		      .o_tready(s_axis_data_tready),
		      .o_tvalid(s_axis_data_tvalid),

		      .o_lines_left(lines_left)
		      );

endmodule
